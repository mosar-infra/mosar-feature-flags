# feature-flags/locals.tf

locals {
  subnets = {
    private = [for s in data.aws_subnet.private : s.id]
    public  = [for s in data.aws_subnet.public : s.id]
  }
}

locals {
  cidr_blocks = {
    private = [for s in data.aws_subnet.private : s.cidr_block]
    public  = [for s in data.aws_subnet.public : s.cidr_block]
  }
}

locals {
  roles = {
    feature_flags_task = {
      role = {
        name               = "feature-flags-ecs-task-role-${var.environment}"
        description        = "feature-flags role tasks"
        assume_role_policy = file("./iam_spec_files/assume_role_policy_ecs.json")
      }
      policy = {
        name        = "feature-flags-ecs-task-policy-${var.environment}"
        description = "Policy for Game task role"
        policy = templatefile("./iam_spec_files/feature_flags_ecs_task_policy.tpl", {
          DB_USER = local.db.flagr_user
          DB_ID   = local.db.db_id
        })
      }
      policy_attachment = {
        name = "feature-flags-ecs-task-policy-attachment"
      }
    }
    feature_flags_task_execution = {
      role = {
        name               = "feature-flags-ecs-task-execution-role-${var.environment}"
        description        = "role to execute the feature_flags ecs tasks"
        assume_role_policy = file("./iam_spec_files/assume_role_policy_ecs.json")
      }
      policy = {
        name        = "feature-flags-ecs-task-execution-policy-${var.environment}"
        description = "Policy to execute ecs feature_flags tasks"
        policy      = file("${path.root}/iam_spec_files/feature_flags_ecs_task_execution_policy.json")
      }
      policy_attachment = {
        name = "feature-flags-ecs-task-execution-policy-attachment"
      }
    }
  }
}

locals {
  security_groups = {
    feature_flags = {
      name        = "feature_flags_sg"
      vpc_id      = data.aws_vpc.vpc.id
      description = "security group access to the feature_flags service"
      ingress_sg  = {}
      ingress_cidr = {
        http = {
          from        = 18000
          to          = 18000
          protocol    = "tcp"
          cidr_blocks = local.cidr_blocks.private
        }
      }
      egress = {
        http = {
          from        = 0
          to          = 0
          protocol    = -1
          cidr_blocks = ["0.0.0.0/0"]
        }
      }
    }
    feature_flags_lb = {
      name         = "feature_flags_lb_sg"
      vpc_id       = data.aws_vpc.vpc.id
      description  = "security group access to the feature flags service from the lb for UI access"
      ingress_cidr = {}
      ingress_sg = {
        http = {
          from            = 18000
          to              = 18000
          protocol        = "tcp"
          security_groups = [data.aws_security_group.mosar_lb.id]
        }
      }
      egress = {
        http = {
          from        = 0
          to          = 0
          protocol    = -1
          cidr_blocks = ["0.0.0.0/0"]
        }
      }
    }

  }
}

locals {
  feature_flags_names = {
    name           = "feature_flags"
    container_name = "feature-flags-${var.environment}"
    namespace_id   = regex("/(?P<id>.*)", data.aws_route53_zone.zone.linked_service_description).id
    volume_name    = "efs_feature_flags"
    domain         = "mosar-${var.environment}-feature-flags.inquisitive.nl"
  }
}

locals {
  db = {
    db_id      = data.aws_db_instance.database.db_instance_identifier
    flagr_user = data.aws_secretsmanager_secret_version.flagr_user.secret_string
    flagr_pass = data.aws_secretsmanager_secret_version.flagr_pass.secret_string
    db_host    = data.aws_db_instance.database.address
  }
}

locals {
  apps = {
    feature_flags = {
      name                    = "mosar-feature-flags"
      desired_count           = 1
      container_definitions   = templatefile("./container_spec_files/mosar-feature-flags-container.tpl", local.container_vars)
      subnet_ids              = local.subnets.private
      security_groups         = concat(module.security_groups.security_groups["feature_flags"].*.id, module.security_groups.security_groups["feature_flags_lb"].*.id)
      assign_public_ip        = false
      family                  = "mosar-feature-flags-family"
      use_load_balancer       = true
      lb_target_group_arn     = aws_lb_target_group.tg.arn
      lb_container_name       = local.feature_flags_names.container_name
      lb_container_port       = 18000
      ecs_task_execution_role = module.iam.role["feature_flags_task_execution"]
      ecs_task_role           = module.iam.role["feature_flags_task"]
      cpu                     = 256
      memory                  = 512
      cluster                 = data.aws_ecs_cluster.cluster
      discovery_service = {
        name              = local.feature_flags_names.name
        namespace_id      = local.feature_flags_names.namespace_id
        ttl               = 30
        failure_threshold = 5
      }
      autoscaling = {
        max_capacity = 3
        min_capacity = 1
      }
      autoscaling_policies = {
        memory = {
          name                   = "feature_flags_memory_autoscaling_policy"
          predefined_metric_type = "ECSServiceAverageMemoryUtilization"
          target_value           = 80
        }
        cpu = {
          name                   = "feature_flags_cpu_autoscaling_policy"
          predefined_metric_type = "ECSServiceAverageCPUUtilization"
          target_value           = 60
        }
      }
    }
  }
}

locals {
  container_vars = {
    FEATURE_FLAGS_IMAGE      = var.feature_flags_image
    FEATURE_FLAGS_IMAGE_TAG  = var.feature_flags_image_tag
    ENVIRONMENT              = var.environment
    MOSAR_LOG_GROUP_NAME     = "mosar-logs-${var.environment}"
    FLAGR_DB_DBCONNECTIONSTR = "${local.db.flagr_user}:${local.db.flagr_pass}@tcp(${local.db.db_host}:3306)/flagr?parseTime=true"
    FLAGR_DB_DBDRIVER        = "mysql"
    CONTAINER_NAME           = local.feature_flags_names.container_name
  }
}

locals {
  private_subnet_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
    },
    {
      name   = "tag:Name"
      values = ["*private*"]
  }]
}

locals {
  public_subnet_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
    },
    {
      name   = "tag:Name"
      values = ["*public*"]
  }]
}

locals {
  env_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
  }]
}
