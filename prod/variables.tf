# feature_flags/variables.tf

variable "environment" {
  default = "prod"
}
variable "home_ips" {}
variable "managed_by" {
  default = "feature_flags"
}
variable "feature_flags_image" {}
variable "feature_flags_image_tag" {}
