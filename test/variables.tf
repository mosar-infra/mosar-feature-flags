# feature_flags/variables.tf

variable "environment" {
  default = "test"
}
variable "home_ips" {}
variable "managed_by" {
  default = "feature_flags"
}
variable "feature_flags_image" {}
variable "feature_flags_image_tag" {}
