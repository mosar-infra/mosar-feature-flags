# feature_flag / datasources

data "aws_region" "current" {}

data "aws_ecs_cluster" "cluster" {
  cluster_name = "mosar-ecs-cluster-${var.environment}"
}

data "aws_subnets" "private" {
  dynamic "filter" {
    for_each = local.private_subnet_filter
    content {
      name   = filter.value.name
      values = filter.value.values
    }
  }
}

data "aws_subnet" "private" {
  for_each = toset(data.aws_subnets.private.ids)
  id       = each.value
}

data "aws_subnets" "public" {
  dynamic "filter" {
    for_each = local.public_subnet_filter
    content {
      name   = filter.value.name
      values = filter.value.values
    }
  }
}
data "aws_lb" "lb" {
  name = "mosar-lb-${var.environment}"
  tags = {
    Environment = var.environment
    ManagedBy   = "ecs-cluster"
  }
}

data "aws_lb_listener" "lb_listener" {
  load_balancer_arn = data.aws_lb.lb.arn
  port              = 443
}

data "aws_subnet" "public" {
  for_each = toset(data.aws_subnets.public.ids)
  id       = each.value
}

data "aws_vpc" "vpc" {
  dynamic "filter" {
    for_each = local.env_filter
    content {
      name   = filter.value.name
      values = filter.value.values
    }
  }
}

data "aws_security_group" "mosar_lb" {
  name = "mosar_lb_sg"
  tags = {
    Environment = var.environment
  }
}

data "aws_db_instance" "database" {
  db_instance_identifier = "mosar-db-${var.environment}"
}

data "aws_secretsmanager_secret" "flagr_user" {
  name = "db-user-flagr"
}

data "aws_secretsmanager_secret_version" "flagr_user" {
  secret_id = data.aws_secretsmanager_secret.flagr_user.id
}

data "aws_secretsmanager_secret" "flagr_pass" {
  name = "db-pass-flagr"
}

data "aws_secretsmanager_secret_version" "flagr_pass" {
  secret_id = data.aws_secretsmanager_secret.flagr_pass.id
}
# TODO: use this when this ticket is resolved: https://github.com/hashicorp/terraform-provider-aws/issues/20313
# data "aws_service_discovery_dns_namespace" "namespace" {
#   name = "mosar.local"
#   type = "DNS_PRIVATE"
# }

# TODO: when above is used, remove this workaround:
data "aws_route53_zone" "zone" {
  name   = "mosar.local"
  vpc_id = data.aws_vpc.vpc.id
}

data "aws_acm_certificate" "feature_flags" {
  domain   = local.feature_flags_names.domain
  statuses = ["ISSUED"]
}
