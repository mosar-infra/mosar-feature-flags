output "iam" {
  value     = module.iam
  sensitive = true
}

output "app" {
  value     = module.ecs-app
  sensitive = true
}

output "target_group" {
  value = aws_lb_target_group.tg
}

output "listener_rule" {
  value = aws_lb_listener_rule.rule
}

output "security_groups" {
  value = module.security_groups
}


