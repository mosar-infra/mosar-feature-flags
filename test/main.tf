# feature-flags/main.tf

module "ecs-app" {
  source      = "git::https://gitlab.com/mosar-infra/tf-module-ecs-app.git?ref=tags/v1.0.6"
  for_each    = local.apps
  app         = each.value
  environment = var.environment
  managed_by  = var.managed_by
}

module "security_groups" {
  source          = "git::https://gitlab.com/mosar-infra/tf-module-security-groups.git?ref=tags/v1.2.2"
  environment     = var.environment
  managed_by      = var.managed_by
  security_groups = local.security_groups
}

module "iam" {
  source      = "git::https://gitlab.com/mosar-infra/tf-module-iam.git?ref=tags/v3.1.2"
  roles       = local.roles
  environment = var.environment
  managed_by  = var.managed_by
}

resource "aws_lb_target_group" "tg" {
  name        = "feature-flags-lb-tg"
  port        = 18000
  protocol    = "HTTP"
  vpc_id      = data.aws_vpc.vpc.id
  target_type = "ip"
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 4
    path                = "/api/v1/flags"
    timeout             = 10
    interval            = 15
  }

  tags = {
    ManagedBy   = var.managed_by
    Environment = var.environment
  }
}

resource "aws_lb_listener_rule" "rule" {
  listener_arn = data.aws_lb_listener.lb_listener.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tg.arn
  }

  condition {
    host_header {
      values = [local.feature_flags_names.domain]
    }
  }

  condition {
    source_ip {
      values = var.home_ips
    }
  }

  tags = {
    ManagedBy   = var.managed_by
    Environment = var.environment
  }
}

resource "aws_lb_listener_certificate" "feature_flags" {
  listener_arn    = data.aws_lb_listener.lb_listener.arn
  certificate_arn = data.aws_acm_certificate.feature_flags.arn
}


